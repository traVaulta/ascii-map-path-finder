package dev.travaulta.finder;

import dev.travaulta.finder.domain.AsciiNode;
import dev.travaulta.finder.domain.Direction;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static dev.travaulta.finder.data.ExamplesASCIIMap.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestASCIIMapPathFinder {
    @Test
    public void should_find_end_position_properly() {
        String[][] givenMatrix = new String[][] {
                { "@", "-", "-", "-", "A", "-", "-", "-", "+" },
                { " ", " ", " ", " ", " ", " ", " ", " ", "|" },
                { "x", "-", "B", "-", "+", " ", " ", " ", "|" },
                { " ", " ", " ", " ", "|", " ", " ", " ", "|" },
                { " ", " ", " ", " ", "+", "-", "-", "-", "C" }
        };
        var actual = ASCIIMapPathFinder.findEndPosition(givenMatrix, 3, 3);

        Assertions.assertThat(actual).containsExactly(0, 2);
    }

    @Test
    public void should_be_able_to_turn_for_plus_symbol() {
        var currentNode = new AsciiNode("+", 4, 2);
        var actual = ASCIIMapPathFinder.checkIfCanTurn(currentNode);

        Assertions.assertThat(actual).isTrue();
    }

    @Test
    public void should_be_able_to_turn_any_alpha_character() {
        var currentNode = new AsciiNode("a", 4, 2);
        var actual = ASCIIMapPathFinder.checkIfCanTurn(currentNode);

        Assertions.assertThat(actual).isTrue();
    }

    @Test
    public void should_not_be_able_to_turn() {
        var currentNode = new AsciiNode("-", 3, 2);
        var actual = ASCIIMapPathFinder.checkIfCanTurn(currentNode);

        Assertions.assertThat(actual).isFalse();
    }

    @Test
    public void should_connect_nodes_properly() {
        var nodes = Arrays.asList(
                new AsciiNode("@", 0, 0),
                new AsciiNode("-", 1, 0),
                new AsciiNode("-", 2, 0),
                new AsciiNode("-", 3, 0),
                new AsciiNode("A", 4, 0),
                new AsciiNode("-", 5, 0),
                new AsciiNode("-", 6, 0),
                new AsciiNode("-", 7, 0),
                new AsciiNode("+", 8, 0)
        );

        ASCIIMapPathFinder.connectNodes(nodes);

        Assertions
            .assertThat(nodes)
            .matches(data -> data.get(1).getRight() == data.get(2))
            .matches(data -> data.get(4).getRight() == data.get(5));
    }

    @Test
    public void should_build_nodes() {
        String[][] givenCharMatrix = new String[][] {
                { "@", "-", "-", "-", "A", "-", "-", "-", "+" },
                { " ", " ", " ", " ", " ", " ", " ", " ", "|" },
                { "x", "-", "B", "-", "+", " ", " ", " ", "|" },
                { " ", " ", " ", " ", "|", " ", " ", " ", "|" },
                { " ", " ", " ", " ", "+", "-", "-", "-", "C" }
        };
        List<AsciiNode> nodes = new ArrayList<>();
        int colCount = 9, rowCount = 5;

        ASCIIMapPathFinder.buildNodes(nodes, givenCharMatrix, colCount, rowCount);

        Assertions.assertThat(nodes).hasSize(23);
    }

    @Test
    public void should_find_nodes_path() {
        var nodes = Arrays.asList(
                new AsciiNode("@", 0, 0),
                new AsciiNode("-", 1, 0),
                new AsciiNode("-", 2, 0),
                new AsciiNode("-", 3, 0),
                new AsciiNode("A", 4, 0),
                new AsciiNode("-", 5, 0),
                new AsciiNode("-", 6, 0),
                new AsciiNode("-", 7, 0),
                new AsciiNode("+", 8, 0),
                new AsciiNode("x", 9, 0)
        );

        ASCIIMapPathFinder.connectNodes(nodes);

        List<AsciiNode> path = new ArrayList<>();

        ASCIIMapPathFinder.findPath(nodes.get(0), null, Direction.RIGHT, path);

        Assertions.assertThat(path).hasSize(10);
        Assertions.assertThat(getPathStr(path)).isEqualTo("@---A---+x");
    }

    @Test
    public void should_throw_on_find_path_when_fake_turn_for_sample_input_13() throws Exception {
        ASCIIMapPathFinder.execute(FAKE_TURN_EXAMPLE);
    }

    @Test
    public void should_throw_on_find_path_when_multiple_starting_paths_for_sample_input_12() throws Exception {
        ASCIIMapPathFinder.execute(MULTIPLE_STARTING_PATHS_EXAMPLE);
    }

    @Test
    public void should_throw_on_find_path_when_broken_path_for_sample_input_11() throws Exception {
        ASCIIMapPathFinder.execute(BROKEN_PATH_EXAMPLE);
    }

    @Test
    public void should_throw_on_find_path_when_fork_left_and_right_present_for_sample_input_10() {
        assertThrows(Exception.class, () -> ASCIIMapPathFinder.execute(T_FORK_EXAMPLE));
    }

    @Test
    public void should_throw_on_find_path_when_multiple_end_characters_present_for_sample_input_9() {
        assertThrows(Exception.class, () -> ASCIIMapPathFinder.execute(MULTIPLE_ENDS_EXAMPLE_INVALID));
    }

    @Test
    public void should_throw_on_find_path_when_multiple_start_characters_present_for_sample_input_8() {
        assertThrows(Exception.class, () -> ASCIIMapPathFinder.execute(MULTIPLE_STARTS_EXAMPLE_INVALID));
    }

    @Test
    public void should_throw_on_find_path_when_missing_end_character_for_sample_input_7() {
        assertThrows(Exception.class, () -> ASCIIMapPathFinder.execute(NO_END_EXAMPLE_INVALID));
    }

    @Test
    public void should_throw_on_find_path_when_missing_start_character_for_sample_input_6() {
        assertThrows(Exception.class, () -> ASCIIMapPathFinder.execute(NO_START_EXAMPLE_INVALID));
    }

    @Test
    @Disabled
    public void should_find_path_while_keeping_direction_for_sample_input_5() throws Exception {
        var path = ASCIIMapPathFinder.execute(KEEP_DIRECTION_EXAMPLE);
        Assertions
                .assertThat(getPathStr(path))
                .isEqualTo("@B+++B|+-L-+A+++A-+Hx");
    }

    @Test
    public void should_find_path_while_skipping_letter_on_same_location_for_sample_input_4() throws Exception {
        var path = ASCIIMapPathFinder.execute(LETTER_SAME_LOCATION_EXAMPLE);
        Assertions
                .assertThat(getPathStr(path))
                .isEqualTo("@-G-O-+|+-+|O||+-O-N-+|I|+-+|+-I-+|ES|x");
    }

    @Test
    public void should_find_path_while_letters_found_on_turns_for_sample_input_3() throws Exception {
        var path = ASCIIMapPathFinder.execute(LETTER_TURNS_EXAMPLE);
        Assertions
                .assertThat(getPathStr(path))
                .isEqualTo("@---A---+|||C---+|+-B-x");
    }

    @Test
    public void should_find_path_while_going_straight_through_intersection_for_sample_input_2() throws Exception {
        var path = ASCIIMapPathFinder.execute(INTERSECTIONS_EXAMPLE);
        Assertions
                .assertThat(getPathStr(path))
                .isEqualTo("@|A+---B--+|+--C-+|-||+---D--+|x");
    }

    @Test
    public void should_find_path_for_sample_input_1() throws Exception {
        var path = ASCIIMapPathFinder.execute(BASIC_EXAMPLE);
        Assertions
                .assertThat(getPathStr(path))
                .isEqualTo("@---A---+|C|+---+|+-B-x");
    }

    @Test
    public void should_be_able_to_turn_when_symbol_of_current_is_plus() {
        var givenNode = new AsciiNode("+", 0, 0);

        Assertions.assertThat(ASCIIMapPathFinder.checkIfCanTurn(givenNode)).isTrue();
    }

    @Test
    public void should_not_be_able_to_turn_when_symbol_of_current_is_minus() {
        var givenNode = new AsciiNode("-", 0, 0);

        Assertions.assertThat(ASCIIMapPathFinder.checkIfCanTurn(givenNode)).isFalse();
    }

    @Test
    public void should_not_be_able_to_turn_when_symbol_of_current_is_pipe() {
        var givenNode = new AsciiNode("|", 0, 0);

        Assertions.assertThat(ASCIIMapPathFinder.checkIfCanTurn(givenNode)).isFalse();
    }

    private static String getPathStr(List<AsciiNode> path) {
        return path.stream().map(AsciiNode::getSymbol).collect(Collectors.joining());
    }
}
