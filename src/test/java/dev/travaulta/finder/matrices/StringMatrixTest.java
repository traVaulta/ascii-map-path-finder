package dev.travaulta.finder.matrices;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class StringMatrixTest {
    static final String SEPARATOR = System.getProperty("os.name").contains("Windows") ? "\r\n" : "\n";

    static final String[][] MOCK_CHAR_MATRIX = {
            { " ", " ", "x" },
            { "@", " ", "|" },
            { "+", "-", "+" }
    };

    @Test
    public void should_print_int_matrix_properly() throws Exception {
        ByteArrayOutputStream mockOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(mockOutput));

        StringMatrix.printChars2D(MOCK_CHAR_MATRIX, 3, 3);
        mockOutput.flush();
        var actualOutput = mockOutput.toString();

        var outputSections = actualOutput.split(SEPARATOR);
        assertEquals("    x ", outputSections[0]);
        assertEquals("@   | ", outputSections[1]);
        assertEquals("+ - + ", outputSections[2]);
    }

    @Test
    public void should_extract_char_matrix_from_string() {
        var actual = StringMatrix.extractCharsMatrix("""
              x
            @ |
            +-+""", 3, 3);
        assertArrayEquals(MOCK_CHAR_MATRIX[0], actual[0]);
        assertArrayEquals(MOCK_CHAR_MATRIX[1], actual[1]);
        assertArrayEquals(MOCK_CHAR_MATRIX[2], actual[2]);
    }
}
