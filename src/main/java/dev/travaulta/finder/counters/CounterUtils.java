package dev.travaulta.finder.counters;

import java.util.Arrays;
import java.util.Comparator;

public class CounterUtils {
    public static int getRowCount(String source) {
        return source.split("\n").length;
    }

    public static int getColCount(String source) {
        return Arrays.stream(source.split("\n"))
                .max(Comparator.comparingInt(String::length))
                .orElse("").length();
    }

    public static int countFromSymbol(String source, String s) {
        return (int) Arrays.stream(source.split("")).filter(c -> c.equals(s)).count();
    }
}
