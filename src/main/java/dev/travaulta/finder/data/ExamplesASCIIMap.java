package dev.travaulta.finder.data;

public class ExamplesASCIIMap {
    final public static String BASIC_EXAMPLE = """
            @---A---+
                    |
            x-B-+   C
                |   |
                +---+
              """;
    final public static String INTERSECTIONS_EXAMPLE = """
            @
            | +-C--+
            A |    |
            +---B--+
              |      x
              |      |
              +---D--+
              """;
    final public static String LETTER_TURNS_EXAMPLE = """
            @---A---+
                    |
            x-B-+   |
                |   |
                +---C
              """;
    final public static String LETTER_SAME_LOCATION_EXAMPLE = """
                +-O-N-+
                |     |
                |   +-I-+
            @-G-O-+ | | |
                | | +-+ E
                +-+     S
                        |
                        x
               """;
    final public static String KEEP_DIRECTION_EXAMPLE = """
             +-L-+
             |  +A-+
            @B+ ++ H
             ++    x
                """;
    // TODO: invalid exmaples
    final public static String NO_START_EXAMPLE_INVALID = """
               -A---+
                    |
            x-B-+   C
                |   |
                +---+
              """;
    final public static String NO_END_EXAMPLE_INVALID = """
            @--A---+
                   |
             B-+   C
               |   |
               +---+
             """;
    final public static String MULTIPLE_STARTS_EXAMPLE_INVALID = """
             @--A-@-+
                    |
            x-B-+   C
                |   |
                +---+
              """;
    final public static String MULTIPLE_ENDS_EXAMPLE_INVALID = """
             @--A---+
                    |
            x-Bx+   C
                |   |
                +---+
              """;
    final public static String T_FORK_EXAMPLE = """
                 x-B
                   |
            @--A---+
                   |
              x+   C
               |   |
               +---+
             """;
    final public static String BROKEN_PATH_EXAMPLE = """
            @--A-+
                 |
                  
                 B-x
            """;
    final public static String MULTIPLE_STARTING_PATHS_EXAMPLE = """
            -B-@-A-x
              """;
    final public static String FAKE_TURN_EXAMPLE = """
            @-A-+-B-x
              """;
}
