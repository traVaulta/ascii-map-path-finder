package dev.travaulta.finder;

import dev.travaulta.finder.counters.CounterUtils;
import dev.travaulta.finder.domain.AsciiNode;
import dev.travaulta.finder.domain.Direction;
import dev.travaulta.finder.matrices.StringMatrix;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static dev.travaulta.finder.data.ExamplesASCIIMap.*;

public class ASCIIMapPathFinder {
    static final String TITLE = "Path Finder Algorithm";
    static final String START_CHAR = "@";
    static final String END_CHAR = "x";

    public static void main(String[] args) throws Exception {
        System.out.println(TITLE);
        var path = execute(LETTER_SAME_LOCATION_EXAMPLE);
        System.out.println(path.stream().map(AsciiNode::getSymbol).collect(Collectors.joining()));
        System.out.println(path.stream()
                .filter(ASCIIMapPathFinder::isAlphaCharacter)
                .map(AsciiNode::getSymbol)
                .filter(symbol -> !symbol.equals("x"))
                .collect(Collectors.joining())
        );
    }

    public static List<AsciiNode> execute(String sample) throws Exception {
        int startCharsCount = CounterUtils.countFromSymbol(sample, START_CHAR);
        int endCharsCount = CounterUtils.countFromSymbol(sample, END_CHAR);
        if (startCharsCount == 0 || startCharsCount > 1 || endCharsCount == 0 || endCharsCount > 1) {
            throw new Exception("Error");
        }

        int colCount = CounterUtils.getColCount(sample);
        int rowCount = CounterUtils.getRowCount(sample);

        String[][] chars2D = StringMatrix.extractCharsMatrix(sample, colCount, rowCount);

        //StringMatrix.printChars2D(chars2D, colCount, rowCount);

        List<AsciiNode> nodes = new ArrayList<>();
        buildNodes(nodes, chars2D, colCount, rowCount);

        connectNodes(nodes);

        List<AsciiNode> path = new ArrayList<>();
        findPath(nodes.get(0), null, Direction.RIGHT, path);

        return path;
    }

    public static void findPath(AsciiNode current, AsciiNode previous, Direction direction, List<AsciiNode> path) {
        var alreadyContained = path.contains(current);
        if (!alreadyContained) {
            path.add(current);
        }

        if (current.getSymbol().equals(END_CHAR)) return;

        Consumer<AsciiNode> resolverPredicate = node -> {
            if (Objects.nonNull(node) && node != previous) {
                if (alreadyContained) {
                    path.add(current);
                }
                findPath(node, current, direction, path);
            } else if (checkIfCanTurn(current)) {
                if (!alreadyContained) {
                    path.remove(current);
                }
                findPath(current, previous, direction.successor(), path);
            }
        };

        switch (direction) {
            case RIGHT -> resolverPredicate.accept(current.getRight());
            case DOWN -> resolverPredicate.accept(current.getDown());
            case LEFT -> resolverPredicate.accept(current.getLeft());
            case TOP -> resolverPredicate.accept(current.getTop());
        }
    }

    public static void connectNodes(List<AsciiNode> nodes) {
        final int k = nodes.size();
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < k; j++) {
                if (i != j) {
                    int diffX = nodes.get(j).getX() - nodes.get(i).getX();
                    int diffY = nodes.get(j).getY() - nodes.get(i).getY();

                    boolean isLeftNeighbour = diffX == -1 && diffY == 0;
                    boolean isRightNeighbour = diffX == 1 && diffY == 0;
                    boolean isTopNeighbour = diffY == -1 && diffX == 0;
                    boolean isBottomNeighbour = diffY == 1 && diffX == 0;

                    if (isLeftNeighbour) {
                        nodes.get(i).setLeft(nodes.get(j));
                    } else if (isRightNeighbour) {
                        nodes.get(i).setRight(nodes.get(j));
                    } else if (isTopNeighbour) {
                        nodes.get(i).setTop(nodes.get(j));
                    } else if (isBottomNeighbour) {
                        nodes.get(i).setDown(nodes.get(j));
                    }
                }
            }
        }
    }

    public static void buildNodes(
            List<AsciiNode> nodes, String[][] chars2D,
            int colCount, int rowCount
    ) {
        for (int i = 0; i < colCount; i++) {
            for (int j = 0; j < rowCount; j++) {
                if (!chars2D[j][i].equals(" ")) {
                    var current = new AsciiNode(chars2D[j][i], i, j);
                    nodes.add(current);
                }
            }
        }
    }

    public static boolean checkIfCanTurn(AsciiNode current) {
        return current.getSymbol().equals("+") || current.getSymbol().equals(START_CHAR) || isAlphaCharacter(current);
    }

    public static boolean isAlphaCharacter(AsciiNode current) {
        var currentSymbol = current.getSymbol().toLowerCase();
        var symbol = Character.valueOf(currentSymbol.charAt(0));
        return symbol.hashCode() >= 97 && symbol.hashCode() <= 122;
    }

    public static int[] findEndPosition(String[][] chars2D, int colCount, int rowCount) {
        int endRow = 0, endCol = 0;

        for (int k = 0; k < rowCount; k++) {
            for (int p = 0; p < colCount; p++) {
                if (chars2D[k][p].equals(END_CHAR)) {
                    endRow = k;
                    endCol = p;
                }
            }
        }

        return new int[]{endCol, endRow};
    }
}
