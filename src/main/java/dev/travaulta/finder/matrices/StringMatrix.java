package dev.travaulta.finder.matrices;

public class StringMatrix {
    public static void printChars2D(String[][] chars2D, int colCount, int rowCount) {
        for (int y = 0; y < rowCount; y++) {
            for (int x = 0; x < colCount; x++) {
                System.out.print(chars2D[y][x] + " ");
            }
            System.out.println();
        }
    }

    public static String[][] extractCharsMatrix(String source, int colCount, int rowCount) {
        var rows = source.split("\n");
        String[][] chars2D = new String[rowCount][colCount];
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < colCount; j++) {
                if (rows[i].length() <= j) {
                    chars2D[i][j] = " ";
                } else {
                    chars2D[i][j] = String.valueOf(rows[i].charAt(j));
                }
            }
        }
        return chars2D;
    }
}
