package dev.travaulta.finder.domain;

public enum Direction {
    DOWN(1, "DOWN"),
    RIGHT(2, "RIGHT"),
    TOP(3, "TOP"),
    LEFT(4, "LEFT");

    public final int id;
    public final String description;

    Direction(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public Direction successor() {
        return enumForId(id == 4 ? 1 : id + 1);
    }

    public Direction enumForId(int i) {
        return switch (i) {
            case 1 -> Direction.DOWN;
            case 2 -> Direction.RIGHT;
            case 3 -> Direction.TOP;
            case 4 -> Direction.LEFT;
            default -> null;
        };
    }
}
