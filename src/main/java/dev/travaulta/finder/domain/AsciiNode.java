package dev.travaulta.finder.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class AsciiNode {
    private String symbol;
    private int x;
    private int y;
    private AsciiNode left;
    private AsciiNode right;
    private AsciiNode top;
    private AsciiNode down;

    public AsciiNode(String symbol, int x, int y) {
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "AsciiNode{" +
                "symbol='" + symbol + '\'' +
                ", x=" + x +
                ", y=" + y +
                /*left != null ? ", left=" + left.getSymbol() : "" +
                right != null ? ", right=" + right.getSymbol() : "" +
                top != null ? ", top=" + top.getSymbol() : "" +
                down != null ? ", down=" + down.getSymbol() : "" +*/
                '}';
    }
}
